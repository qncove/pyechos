# pyechos #

pyechos: very minimalistic, simple and easy to use Adobe EchoSign Wrapper

### What is it good for? ###

* Signing Documents

### How do I use it? ###

```
#!python

>>> from pyechos import api


```

### Roadmap 

- Working on this

If you have any questions, just email me at [juan@qn.co.ve](mailto:juan@qn.co.ve) or follow me on twitter: [@vladjanicek](http://twitter.com/vladjanicek)

This library is licensed under the [GNU Affero General Public License](http://www.gnu.org/licenses/agpl-3.0.html)