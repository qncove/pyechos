# -*- coding: UTF-8 -*-
#
# Juan Hernandez, 2013 2014
# EchoSign API Functions
#

import requests

import os

api_url = 'https://secure.echosign.com/api/rest/v2'


def get_token(api_key, application_secret, application_id):
    """Get Auth Token

    :param api_key: API Key
    :param application_secret: app secret
    :param application_id: app id
    :return: Dictionary with accessToken and expiresIn keys

    """
    headers = {
        'Content-Type': 'application/json;charset=UTF-8'
    }
    data = """{
        "userCredentials": {
            "apiKey": "%s"
        },
        "applicationCredentials": {
            "applicationSecret": "%s",
            "applicationId": "%s"
        }
    }""" % (api_key, application_secret, application_id)

    return requests.post(api_url + '/auth/tokens',
                         headers=headers, data=data).json()


def send_document(access_token, file_path=None, binary_object=None):
    """Uploads document to EchoSign and returns its ID

    :param access_token: EchoSign Access Token
    :param file_path: Absolute or relative path to File
    :param binary_object: if binary object is given, file path will be ignored.
                        Binary objects have priority over file paths
    :return string: Document ID

    """
    headers = {
        'Access-Token': access_token,
    }
    data = {
        'Mime-Type': 'application/pdf',
        'File-Name': 'hihair_subscription_contract.pdf'
    }
    url = api_url + '/transientDocuments'

    if not file_path and not binary_object:
        raise Exception('You must either have a file path or a binary object')

    if not binary_object:
        files = {'File': open(file_path, 'rb')}
    else:
        files = {'File': binary_object}
    return requests.post(url, headers=headers, data=data,
                         files=files).json().get('transientDocumentId')


def get_agreements(access_token):
    headers = {'Access-Token': access_token}
    url = api_url + '/agreements'
    return requests.get(url, headers=headers).json()


def get_agreement(access_token, agreement_id):
    headers = {'Access-Token': access_token}
    url = api_url + '/agreements/' + agreement_id
    print url
    return requests.get(url, headers=headers).json()


def get_library_documents(access_token):
    headers = {'Access-Token': access_token}
    url = api_url + '/libraryDocuments'
    return requests.get(url, headers=headers).json()


def send_reminder(access_token, agreement_id, message):
    data = {
        "agreementId": agreement_id,
        "comment": message,
    }
    headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Access-Token': access_token
    }
    url = api_url + '/reminders'
    import json
    return requests.post(url, headers=headers, data=json.dumps(data)).json()


def send_agreement(transient_id,
                   access_token,
                   agreement_name,
                   message,
                   approver_email):
    headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Access-Token': access_token
    }

    data = {
        "documentCreationInfo": {
            "name": agreement_name,
            "signatureType": ["ESIGN"],
            "signatureFlow": "SENDER_SIGNATURE_NOT_REQUIRED",
            "recipients": [{"email": approver_email, "role": "APPROVER"}],
            "message": message,
            "fileInfos": {"transientDocumentId": transient_id},
        },
    }
    import json
    return requests.post(api_url + '/agreements',
                         headers=headers, data=json.dumps(data)).json()

