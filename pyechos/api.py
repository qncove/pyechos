# -*- coding: UTF-8 -*-
#
# Juan Hernandez, 2013 2014

from .libs import api_functions as apif


class EchoSign(object):
    def __init__(self, api_key, application_secret, application_id):
        self.api_key = api_key
        self.application_secret = application_secret
        self.application_id = application_id

    def get_access_token(self):
        return apif.get_token(
            self.api_key,
            self.application_secret,
            self.application_id
        )

    def transient_document(self, access_token,
                           file_path=None, binary_object=None):
        return apif.send_document(access_token, file_path, binary_object)

    def send_agreement(self, transient_id, access_token,
                       agreement_name, message, approver_email):
        return apif.send_agreement(
            transient_id,
            access_token,
            agreement_name,
            message,
            approver_email)

    def get_agreement(self, access_token, agreement_id):
        return apif.get_agreement(access_token, agreement_id)

    def send_reminder(self, access_token, agreement_id, message):
        return apif.send_reminder(access_token, agreement_id, message)

