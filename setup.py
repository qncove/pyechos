#!/usr/bin/env python

from distutils.core import setup

setup(
    name='pyechos',
    version='0.7.1',
    description='Easy Adobe EchoSign Python Wrapper',
    author='Juan Hernandez',
    author_email='juan@qn.co.ve',
    install_requires=['requests>=2.3.0'],
    url='http://qn.co.ve/pyechos/',
    packages=['pyechos', 'pyechos.libs']
)

